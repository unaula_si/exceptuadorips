FROM openjdk:8-jdk-alpine
VOLUME [ "/home" ]
EXPOSE 8089
ENTRYPOINT ["java", "-jar", "-Dspring.profiles.active=release", "/home/exceptuadorips-0.0.1-SNAPSHOT.jar"]
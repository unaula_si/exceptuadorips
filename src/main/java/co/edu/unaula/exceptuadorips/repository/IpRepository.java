package co.edu.unaula.exceptuadorips.repository;

import co.edu.unaula.exceptuadorips.documents.IpDocument;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IpRepository extends ReactiveMongoRepository<IpDocument, String> {
}

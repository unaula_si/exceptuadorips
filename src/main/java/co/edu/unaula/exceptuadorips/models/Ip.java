package co.edu.unaula.exceptuadorips.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@AllArgsConstructor
@Data
@Builder(toBuilder = true)
public class Ip {
    private final String ip;
}
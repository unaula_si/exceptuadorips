package co.edu.unaula.exceptuadorips.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
public class IpDTO {
    @JsonProperty("ip")
    private String ip;
}
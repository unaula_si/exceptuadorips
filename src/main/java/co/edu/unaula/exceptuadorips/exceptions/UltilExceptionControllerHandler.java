package co.edu.unaula.exceptuadorips.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public final class UltilExceptionControllerHandler {

    private UltilExceptionControllerHandler() {
    }

    public static ResponseEntity<ResponseErrorHandler> construirResponseException(Integer status, String message, String code) {
        ResponseErrorHandler responseExceptionHandler = ResponseErrorHandler
                .builder()
                .status(status)
                .message(message)
                .code(code)
                .build();

        return new ResponseEntity<>(responseExceptionHandler, HttpStatus.resolve(status));
    }
}
package co.edu.unaula.exceptuadorips.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class CamposExceptionHandler {
    @ExceptionHandler(UtilsException.class)
    public ResponseEntity<ResponseErrorHandler> camposException(UtilsException exception) {
        return UltilExceptionControllerHandler.construirResponseException(400, exception.getType().getMessage(),"0");
    }
}

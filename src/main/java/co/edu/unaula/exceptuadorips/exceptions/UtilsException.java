package co.edu.unaula.exceptuadorips.exceptions;

public class UtilsException extends RuntimeException {

    public enum Type {
        NO_DISPONIBLE("Servicio No Disponible.");

        private final String message;

        public UtilsException build() {
            return new UtilsException(this);
        }

        Type(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }

    }

    private final Type type;

    private UtilsException(Type type) {
        super(type.message);
        this.type = type;
    }

    public Type getType() {
        return type;
    }

}

package co.edu.unaula.exceptuadorips.converter;

import co.edu.unaula.exceptuadorips.documents.IpDocument;
import co.edu.unaula.exceptuadorips.dto.IpDTO;
import co.edu.unaula.exceptuadorips.models.Ip;

public class IpConverter {
    public static Ip convertirDocumentAModel(IpDocument ipDocument) {
        return Ip.builder()
                .ip(ipDocument.getIp())
                .build();
    }

    public static IpDTO convertirModelADto(Ip ip) {
        return IpDTO.builder()
                .ip(ip.getIp())
                .build();
    }

    public static Ip convertirDtoAModel(IpDTO ip) {
        return Ip.builder()
                .ip(ip.getIp())
                .build();
    }

    public static IpDocument convertirModelADocument(Ip ip) {
        IpDocument ipDocument = new IpDocument();
        ipDocument.setIp(ip.getIp());
        return ipDocument;
    }
}

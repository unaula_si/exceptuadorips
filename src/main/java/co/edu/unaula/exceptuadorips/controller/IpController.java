package co.edu.unaula.exceptuadorips.controller;

import co.edu.unaula.exceptuadorips.controller.base.ControladorBase;
import co.edu.unaula.exceptuadorips.converter.IpConverter;
import co.edu.unaula.exceptuadorips.dto.IpDTO;
import co.edu.unaula.exceptuadorips.service.IpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@RequestMapping(value = "api/")
public class IpController extends ControladorBase {
    private final IpService ipService;

    @Autowired
    public IpController(IpService ipService) {
        this.ipService = ipService;
    }

    @GetMapping(value = "ips")
    public Mono<ResponseEntity<List<IpDTO>>> obtenerIps() {
        return ipService.obtenerIps()
                .map(IpConverter::convertirModelADto)
                .collectList()
                .map(this::validarLista);
    }
}
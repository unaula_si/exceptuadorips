package co.edu.unaula.exceptuadorips.service;

import co.edu.unaula.exceptuadorips.converter.IpConverter;
import co.edu.unaula.exceptuadorips.dto.IpDTO;
import co.edu.unaula.exceptuadorips.exceptions.UtilsException;
import co.edu.unaula.exceptuadorips.models.Ip;
import co.edu.unaula.exceptuadorips.repository.IpRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.List;

@Service
@Slf4j
public class IpService {
    private final IpRepository ipRepository;
    private final WebClient webClient;

    @Value("${application.urlGestionIps}")
    private String urlGistionIps;

    private static final String CONTENT_TYPE_VALUE = "application/json";
    private static final String CONTENT_TYPE = "Content-Type";

    @Autowired
    public IpService(IpRepository ipRepository, WebClient webClient) {
        this.ipRepository = ipRepository;
        this.webClient = webClient;
    }

    public Flux<Ip> obtenerIps() {
        return obtenerIpsTor()
                .collectList()
                .flatMapMany(ipsTor ->
                        ipRepository.findAll()
                                .map(IpConverter::convertirDocumentAModel)
                                .collectList()
                                .flatMapMany(ipsExcluidas ->
                                        filtraIps(ipsTor, ipsExcluidas)));
    }

    private Flux<Ip> filtraIps(List<Ip> ipsTor, List<Ip> ipsExcluidas) {
        return Flux.fromIterable(ipsTor)
                .filter(ipTor ->
                        ipsExcluidas.stream().noneMatch(ipExcluida -> ipExcluida.getIp().equals(ipTor.getIp())));
    }

    private Flux<Ip> obtenerIpsTor(){
        return webClient
                .get()
                .uri(crearURI())
                .header(CONTENT_TYPE, CONTENT_TYPE_VALUE)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .flatMapMany(clientResponse ->
                {
                    if (!clientResponse.statusCode().is2xxSuccessful()){
                        return Mono.error(UtilsException.Type.NO_DISPONIBLE.build());
                    }

                    return clientResponse.bodyToFlux(IpDTO.class);
                })
                .onErrorResume(error -> Mono.error(UtilsException.Type.NO_DISPONIBLE.build()))
                .map(IpConverter::convertirDtoAModel);
    }

    private URI crearURI() {
        return UriComponentsBuilder
                .fromUriString(urlGistionIps)
                .build()
                .toUri();
    }
}
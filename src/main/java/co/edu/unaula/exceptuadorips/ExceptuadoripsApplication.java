package co.edu.unaula.exceptuadorips;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExceptuadoripsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExceptuadoripsApplication.class, args);
	}

}
